# == Class: ckan::ext::lcrnz
#
# Installs the Landcare Research extension.
#
# === Requirements
#  Installs the following extensions.
# * ckan::ext::repeating
# * ckan::ext::ldap
#
# === Parameters
#
# [*uri*]
#   The uri to the ldap server to connect.
#   Example: 'ldap://localhost:389'
#
# [*base_dn*]
#   The ldap base dn to use for user authentication.
#   Example: 'ou=users,dc=landcareresearch,dc=co,dc=nz'
#
# [*source*]
#   The source of the git repository for this extension.
#   Default: 'git://github.com/okfn/ckanext-lcrnz.git'
#
# [*revision*]
#   The revision (or version) of the source to checkout with git.
#   Default: 'master'
#
# [*organization_id*]
# If this is set, users that log in using LDAP will automatically get added
# to the given organization.
#
class ckan::ext::lcrnz (
  $uri,
  $base_dn,
  $organization_id,
  $source   =  'git://github.com/okfn/ckanext-lcrnz.git',
  $revision = 'master',
  ){
  class {'ckan::ext::repeating':
  }
  class {'ckan::ext::ldap':
    uri             => $uri,
    base_dn         => $base_dn,
    organization_id => $organization_id,
  }
  ckan::ext { 'lcrnz':
    source   => $source,
    revision => $revision,
    plugin   => 'lcrnz',
  }
## Custom i18n folder (for replacing "organizations" with "collections")
## Update the path if necessary
#ckan.locale_default = en_NZ
#ckan.i18n_directory = /usr/lib/ckan/default/src/ckanext-lcrnz
}
