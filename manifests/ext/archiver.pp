# == Class: ckan::ext::archiver
#
# Installs the archiver extension.
#
# See the plugin documentation for full details:
#   https://github.com/ckan/ckanext-archiver
#
# == Requirements ==
#
# == Parameters ==
#
# [*ckan_conf*]
#   Default: $ckan::params::ckan_conf
#
# [*paster*]
#   Default: $ckan::params::paster
#
class ckan::ext::archiver(
  $ckan_conf = $ckan::params::ckan_conf,
  $paster    = $ckan::params::paster,
) {
  anchor {'ckan::ext::archiver::begin':}

  include redis
  ckan::ext { 'archiver':
    plugin           => 'archiver',
    revision         => 'master',
    pip_requirements => 'requirements.txt',
    require          => [Anchor['ckan::ext::archiver::begin'],Class['redis']],
  }

  check_run::task { 'install-ckan-archiver-init':
    exec_command => "${ckan::paster} --plugin=ckanext-archiver archiver\
 init --config=${ckan_conf}",
    require      => Ckan::Ext['archiver'],
  }

  concat::fragment { 'ckan_ext_archiver_redis_config':
    target => $ckan::ckan_conf,
    source => 'puppet:///modules/ckan/config/archiver.conf.snippet',
    #At the end, after everything else/in the tail,not in the [app:main] section
    order  => 99,
  }

  # Requires redis pip, but the redis pip is not listed in
  # requirements of the extension.
  ckan::pip_package { 'redis':
    version => '2.10.1',
    require => Check_run::Task[ 'install-ckan-archiver-init'],
  }

  anchor {'ckan::ext::archiver::end':
    require => Ckan::Pip_package['redis'],
  }
}
