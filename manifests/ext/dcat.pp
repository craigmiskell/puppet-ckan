# == Class: ckan::ext::dcat
#
# Installs the dcat extension.
#
# See the plugin documentation for full details:
#
#   https://github.com/ckan/ckanext-dcat
#
class ckan::ext::dcat {

  anchor {'ckan::ext::dcat::begin':}

  ckan::ext { 'dcat':
    plugin           =>
    'dcat dcat_rdf_harvester dcat_json_harvester dcat_json_interface',
    revision         => 'master',
    pip_requirements => 'requirements.txt',
    require          => Anchor['ckan::ext::dcat::begin'],
  }

  anchor {'ckan::ext::dcat::end':
    require => Ckan::Ext['dcat'],
  }
}
