## 2016-07-11 Release 2.2.3

### Summary
Added galleries extension

### Changed
  - Added the galleries extension
  - Added a global parameter for specifying the ckan packaged python library

## 2016-07-07 Release 2.2.2

### Summary
Updated readme

### Changed
  - Added a new class for installing redis for the harvest ext.
  - Added missing ext classes documentation from readme.
  - Added links to each ext class github repo.
  - Removed Authors field in source file headers (as this promotes more of a community).  Using contributors.txt is a better way.

## 2016-07-06 Release 2.2.1

### Summary
Minor fixes

### Changed
  - Fixed a typo in a require statement
  - Fixed geoview ext's configuration providing order and white space.

## 2016-06-23 Release 2.2.0

### Summary
Added email parameters

### Changed
  - Added a parameter for the sender email.
  - Added a parameter for the receiver email.
  - Added a missing file source.
  - Added a missing template.
  - Updated documentation for modules.
  - Updated readme (partial).

## 2016-06-22 Release 2.1.15

### Summary
Merged Community pull requests

### Changed
  - Added a parameter for managing the directory for database backups and restores.
  - Fixed a configuration problem with spatial search.
  - Fixed a bug where an exec runs every puppet run (wsgi_create).
  - Fixed a bug for the location of the paster command.


## 2016-06-21 Release 2.1.14

### Summary
Added geoview extension

### Changed
  - Added geoview extension

## 2016-05-03 Release 2.1.13

### Summary
Updated Spatial Search

### Changed
 - Fixed spatial search problem with templates.

## 2016-04-27 Release 2.1.12

### Summary
Changed pip install path

### Changed
 - Changed from using the ckan python pip to the system packaged pip.
 - Added installation of python2.7 and python-pip for deb versions.

## 2016-04-27 Release 2.1.11

### Summary
Added postgis versions

### Changed
  - Added a parameter to change the postgis version.

## 2016-04-22 Release 2.1.10

### Summary
Ckan 2.5 Support

### Changed
  - Added support for ckan version 2.5.

## 2016-04-18 Release 2.1.9

### Summary
Backup bug fix

### Changed
  - A bug was discovered with the automatic backups.  If the spatial extension was enabled than backups would fail.  This bug has been fixed.

## 2016-03-29 Release 2.1.8

### Summary
Added Private Dataset Extension

### Changed
  - Added a new extension called private dataset
  - Added vagrant testing environment
  - Handling nodejs for Ubuntu 12.04 and 14.04

## 2016-01-11 Release 2.1.7

### Summary
LCR Extension

### Changed
    - Added parameters to change the source repo for the lcr extension.

## 2015-12-09 Release 2.1.6

### Summary
Added scripts for restoring and upgrading

### Changed
  - Added ckan_upgrade_db.bash
  - Added ckan_create_views.bash
  - Added ckan_purge_account.bash (uses expect package)
  - Added recaptcha version.  Changed the default to 2 instead of 1.
  - Fixed postgresql ppa problem bug.
  - Added Ubuntu 14.04 support.
  - Updated the solr default version to 5.3.0

## 2015-10-30 Release 2.1.4

### Summary
Minor changes

### Changed
  - Added the json_formats and xml_formats for the configuration file.
  - Text Formats will always be added to configuration.
  - Changed recaptcha public/private defaults to undef.
  - Updated for future parser compliance (puppet 4)

## 2015-09-xx Release 2.1.0

### Summary
Enabling ckan version 2.3

### Changed
 - Added default_locale param for ckan configuration
 - Added ckan.i18n_directory param for ckan configuration
 - Added support for 2.3 changes like preview vrs new view system
 - Upgraded Landcare Research ext to lcrnz
 - Added repeating extension
 - Added ldap extension
 - Enabled the storage path of the file store to be set by module.
 - Updated to using Solr 5.x
 - Enabled both solr and solr-spatial-field for the backend to spatiel search.
 
## 2015-04-02 Releaes 2.0.1
### Summary
Postgresql Password hash

### Changed
 - The changed to using the postgresql password hash function.
 - Added ckan version specifing the version of ckan to use.

## 2015-03-23 Releaes 2.0.0
### Summary
Solr integration

### NOTE
Due to a change of how the database is initialized, upgrading from previous version of this module will
re-initialize your database!!!!  This means, you need to backup the database before doing this upgrade.

To recover your database use the following command (make sure you sudo su first)

```
paster --plugin=ckan db clean --config=/etc/ckan/default/production.ini ckan_database.pg_dump
paster --plugin=ckan db load --config=/etc/ckan/default/production.ini ckan_database.pg_dump
```

### Changed

 - Added the landcareresearch/solr puppet module.
 - Tested with solr 4.10.3.
 - Database initialization is now moved to a bash script in /usr/local/bin
 - Added a solr schema parameter that instructs which schema version to use.
 - 

## 2015-03-04 Release 1.0.13
### Summary
Account Migration

### Changed
 - Migrated from github to bitbucket
 - Changed ownership of puppetforge account
 - Added openhub badge to readme.
 - Migrated changelog into md format.
 - Changed ckan_run dependancy to landcareresearch account.

### Fixes
  - Output of new CKAN Fact now correctly reports if the /etc/ckan/plugins.out file doesn't exist.

## 2015-02-16 Version 1.0.12
### Summary
Ext Plugin Reworked

### Changes
 - Including extension plugins in the plugins parameter caused the module to fail due to a depedancy on the extension to already be installed prior to ckan started.
   This patch incorperates the plugin into the extension so the extension plugins no longer need to be set via the plugin option.
   It will require 2 puppet runs in order to bring the extensions online though (there isn't any way around this that I know of).
 
## 2015-01-29 Version 1.0.11
### Summary
Hot Fix

### Fixes:
 - Added ckan user password string to ckan.ini.
 - Added puppetforge badge

## 2015-01-28 Version 1.0.10
### Summary
Minor Changes

### Features
 - Added an option to enable event tracking for the Google Analystics which pushes events every hour
 - Added a convience script for creating admin accounts.

### Fixes
 - Cleaned up code
 - Added documentation for ckan::config
 - Added configuration for backup directory
 - Added configuration for ckan database password


## 2015-01-14 Version 1.0.9
### Summary
Added ckanapi support

### Features
 - The ckanapi can now be installed (optional)
 - A helper script for calling ckanapi command line tool

## 2015-01-08 Version 1.0.8
### Summary
Quality Control

### Fixes
 - Cleaned up the code based on puppet-lint (via puppetlinter.com)
 - Enabled github hook for puppetlinter so future commits are checked and reported

## 2015-01-07 Version 1.0.7
### Summary
Fixed a recent security exploit that has effected CKAN sites globally

### Fixes
 - Set the security settings to restrict anonymous users from creating groups and datasets
 - Set the default backend for spatial search to solr in the spatial search extension
 - Changed backups from weekly to daily


## 2014-11-12 Version 1.0.6
### Summary:
CKAN Developers have added a new submodule for managing CKAN extensions.

### Features
 - Added ckan::ext submodule
 - Added Google Analytics extension
 - Added Hierarchy extesion
 - Added New Zealand Landcare extension
 - Added Spatial extension
 - Updated to using metadata.json

## 2014-08-13 Version 1.0.5
### Summary
A CKAN Developer has cleaned up the module and prepared for ckan extensions.

### Features
 - Removed apache module dependency
 - Removed module stage complexity with anchor pattern
 - Removed hard coded security keys from production.ini.erb
 - Added security keys to module parameters

## 2014-07-23 Version 1.0.4
### Summary
Updated Dependencies

### Features
 - Removed the reset_apt module as vagrant can handle updating puppet with a recomended script
 - Updated to the latest apache module which has the necessary changes integrated
 - Changed default value of the apache headers variable
 
## 2014-06-09 Version 1.0.3
### Features
 - Added dependancy for Debian/Ubuntu based systems only
 - Updated readme with new installation parameters
 - Added Server admin's email specified
 - Added recaptcha support
 - Added max_resource_size parameter
 - Added data pusher formats
 - Added apache head configuration (in order to control search engine crawlers) and is optional
 - Added the postgres password as a parameter
 - Added postgres hba configuration to pass in as a parameter

## 2014-06-08 Version 1.0.2
### Summary
Minor bug fixes and supports CKAN 2.2 package version

### Features
 - Fixed a bug if the license was left off, caused errors in datastore
 - Added support for CKAN 2.2 package

## 2014-01-15 Version 1.0.1
### Summary
Minor bug fix when deploying outside of a Vagrant environment

### Features
 - Added a parameter to disable the apt reset

### Fixes
 - Removed puppetlabs-apt and apt::ppa which was causing dependency loops if a class outside of ckan required apt::ppa
 - Removed ppa for nodejs & ubuntugis
 - Added puppetlabs-nodejs class
 - Added dependency for puppetlabs-nodejs

## Version 1.0.0
### Summary
Initial Release.
