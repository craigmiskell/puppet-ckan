# == Class ckan::install
#
# Install Node and NPM (which comes with Node)
#
class ckan::install::nodejs {

  anchor{'ckan::install::nodejs::begin':}

  # Install Node and NPM (which comes with Node)

  # problems with Ubuntu 12.04
  if $::operatingsystem == 'Ubuntu' and $::lsbdistrelease == '12.04' {


  # less requires a compile of the css before changes take effect.
  exec { 'Install Less and Nodewatch':
    command => "/usr/bin/curl -sL https://deb.nodesource.com/setup | bash &&\
/usr/bin/apt-get update &&\
/usr/bin/apt-get install nodejs &&\
/usr/bin/npm install less nodewatch",
    cwd     => '/usr/lib/ckan/default/src/ckan',
    creates => '/usr/lib/ckan/default/src/ckan/bin/less',
    before  => Anchor['ckan::install::nodejs::end'],
    require => Anchor['ckan::install::nodejs::begin']
  }

  }else{
    class{'::nodejs':
      nodejs_debug_package_ensure => false,
      nodejs_dev_package_ensure   => false,
      nodejs_package_ensure       => false,
      manage_package_repo         => false,
      npm_package_ensure          => 'present',
      require                     => Anchor['ckan::install::nodejs::begin']
    }
    nodejs::npm{'less':
      target  => '/usr/lib/ckan/default/src/ckan',
      require => Class['::nodejs'],
    }
    nodejs::npm{'nodewatch':
      target  => '/usr/lib/ckan/default/src/ckan',
      before  => Anchor['ckan::install::nodejs::end'],
      require => Nodejs::Npm['less'],
    }
  }

  anchor{'ckan::install::nodejs::end': }
}
