# ![ckan](https://bitbucket.org/landcareresearch/puppet-ckan/downloads/logo-48x48.png) CKAN Puppet Module

[![Puppet Forge](http://img.shields.io/puppetforge/v/landcareresearch/ckan.svg)](https://forge.puppetlabs.com/landcareresearch/ckan)
[![Bitbucket Build Status](http://build.landcareresearch.co.nz/app/rest/builds/buildType%3A%28id%3ALinuxAdmin_PuppetCkan_PuppetCkan%29/statusIcon)](http://build.landcareresearch.co.nz/viewType.html?buildTypeId=LinuxAdmin_PuppetCkan_PuppetCkan&guest=1)
[![Project Stats](https://www.openhub.net/p/puppet-ckan/widgets/project_thin_badge.gif)](https://www.openhub.net/p/puppet-ckan)

## About

This module installs, configures, and manages ckan.
Customizations such as site logo, about page, license, and
customized css are easily configurable from this module.  
The ckan database is automatically backed up every day to 
<backup_dir>/ckan_database.pg_dump.


## Installation

The module can be obtained from the [Puppet Forge](http://forge.puppetlabs.com/landcareresearch/ckan).  The easiest method for installation is to use the
[puppet-module tool](https://github.com/puppetlabs/puppet-module-tool).  Run the following command from your modulepath:

`puppet-module install landcareresearch/ckan`

## Requirements
In order to get all of the dependencies do the following:

 * puppet module install landcareresearch-ckan

Other Requirements

 * Ubuntu 12.04 or 14.04 Operating System.
 

## Administration

Once ckan is setup and installed, its necessary to manage the instance through several command line tools.

### Create Admin Account

Creating an admin account has been simplified with the use of the following command:

`/usr/local/bin/ckan_create_admin.bash <USERNAME>`

### Purge Account

CKAN currently doesn't enable automation in purging users.  You can easily deactivate a user from the web ui.  There is a paster command for marking a user as 'deleted' but that user is still in the system.
In order to completely remove (purge) a user from the system, use the following command:

`/usr/local/bin/ckan_purge_account.bash <USERNAME>`

### Backups
In order to backup the entire ckan installation, the database and the file store need to be included in your backup system.
The following paths should be backed up.

* <backup_dir>/ckan_database.pg_dump
* /var/lib/ckan/default/resources
* /var/lib/ckan/default/storage

### Backup Recovery

In order to recover from a data failure or to just install a new instance do the following as root.

`/usr/local/bin/ckan_recover_db.bash`

### Upgrading

Upgrading between ckan versions is currently not fully automated via puppet.  To upgrade on an existing server, just change the ckan version.
After puppet is run with the new ckan version, there are two scripts that can be used after the upgrade.

* `/usr/local/bin/ckan_upgrade_db.bash`
* `/usr/loca/bin/ckan_upgrade_version.bash`

ckan_upgrade_db will upgrade the database schema between versions.  
ckan_upgrade_version will create the necessary views.


## Classes

### ckan

The class installs and manages a single instance of ckan.  The ckan class is a paramaterized class.

**Required Parameters within `ckan`:**

The parameters listed are required.

#### `site_url`
The url for the ckan site.

#### `param site_title`
The title of the web site. 

#### `site_description`
The description (found in header) of the web site.

#### `site_intro`
The introduction on the landing page.

#### `site_about`
Information on the about page.

#### `plugins`
Contains the ckan plugins to be used by the installation.

DO NOT add extensions that are managed by puppet to this parameter.  As of patch 1.0.12.  All plugins for extensions are now handled in the ext modules.

The following plugins have been tested.

 * stats 
 * text_preview recline_preview 
 * datastore 
 * resource_proxy 
 * pdf_preview

#### `app_instance_id`
The secret password for the app instance which is populated in the production.ini file.
Use paster make-config to generate a config file that contains a new password.
Note, there should be a better method of generating the secrets

#### `beaker_secret`
The secret password for beaker which is populated in the production.ini file.
Use paster make-config to generate a config file that contains a new password.
Note, there should be a better method of generating the secrets

**Optional Parameters within `ckan`:**

The parameters listed in this section can optionally be configured.


#### `site_logo`
The source of the logo.  The logo format requires a png file.

Should be spedified as
`puppet:///<your module>/<image>.png`

#### `email_errors_to`
An e-mail address to send any error reports to.  
Default: undef

#### `email_errors_from`
The from e-mail address to use when sending any error reports.  
Default: undef

#### `license` 
The source to the license file.  The license format requies a json file.
Should be specified as

`puppet:///<your module>/<license file>` and maintained by your module

#### `ckan_version`
Helps identify settings and configuration necessary between the different version of ckan.

```
Valid format: '2.2', '2.3', '2.4' etc.
```

Note, ckan_package_url & ckan_package_filename are not set, than the ckan version will use the package url from ckan.org and the appropriate name.

Defaults to  undef

#### `is_ckan_from_repo` 
A boolean to indicate if the ckan package should be
installed through an already configured repository
setup outside of this module. If using Ubuntu/Deb,
should be able to do "apt-get install python-ckan"
Its the same idea for yum and other package managers (untested).

#### `ckan_package_url` 
If not using a repo, then this url needs to be
specified with the location to download the package.
Note, this is using dpkg so deb/ubuntu only.

#### `ckan_package_filename` 
The filename of the ckan package.

#### `custom_css`
The source to a css file used for the ckan site.  This replaces
the default main.css.  Should be specified as

`puppet:///<your module>/<css filename>` and maintained by your module.

Note, images used in the custom css should be set in custom_imgs.

#### `custom_imgs`
An array of source for the images to be used by the css.
Only required if the custom_css uses new images.

Should be specified as 
`[ 'puppet:///<your module>/<img1>' , 'puppet:///<your module>/<img2>' , ... ]`

#### [`recaptcha_version`](http://docs.ckan.org/en/latest/maintaining/configuration.html#ckan-recaptcha-version)
The version of recaptcha.
Valid options:

 * **1** - Older style with a red box.  The user enters text
 * **2** - Newer style that the user clicks on a checkbox (cleaner).

Default: 2

#### [`recaptcha_publickey`](http://docs.ckan.org/en/latest/maintaining/configuration.html#ckan-recaptcha-publickey)
The public key for recaptcha.  
Default: undef

#### [`recaptcha_privatekey`](http://docs.ckan.org/en/latest/maintaining/configuration.html#ckan-recaptcha-privatekey)
The private key for recaptcha.  
Default: undef

#### `max_resource_size` 
The maximum in megabytes a resource upload can be.  
Default: 100

#### `max_image_size`
The maximum in megabytes an image upload can be.  
Default: 10

#### `datapusher_formats` 
File formats that will be pushed to the DataStore by the DataPusher.  
When adding or editing a resource which links to a file in one of these formats, the DataPusher
will automatically try to import its contents to the DataStore.

#### `default_views`
Defines the resource views that should be created by default when creating
or updating a dataset. From this list only the views that are relevant to a
particular resource format will be created. This is determined by each
individual view.
Default: image_view recline_view

#### ~~`preview_loadable`~~ **Depricated**
Defines the resource formats which should be loaded directly in an iframe tag when previewing 
them if no Data Viewer can preview it. 
Only used in CKAN version 2.2 and below.
As of CKAN 2.3, use default_views

Defaults to 'html htm rdf+xml owl+xml xml n3 n-triples turtle plain atom csv tsv rss txt json'

#### `text_formats` 
Formats used for the text preview

Defaults to not being set in the configuration (since its an empty string)

#### `postgres_pass` 
The password for the postgres user of the database (admin user).

#### `ckan_pass`
The password for the ckan user of the database.

#### `pg_hba_conf_defaults` 
True if use the default hbas and false to configure your own.
This module uses puppetlabs/postgresql so this setting informs the postgresql module
that the hba's should be handled outside of this module.  Requires your own hba configuration.

#### `postgis_version`
The version of postgis to use.

 * **2.1** uses postgresql 9.4
 * **2.2** uses postgresql 9.5

Default: '2.1'  

#### `install_ckanapi`
Installs the ckan api if set to true.  Default is false.
Also installs a helper script in /usr/bin/ckan/ckanapi.bash which
launches ckanapi with the environment setup.
Additional information: https://github.com/ckan/ckanapi

#### `enable_backup`
Backs up the database to /backup/ckan_database.pg_dump.
Default: true

#### `backup_daily`
If backups enabled, sets backups to either daily (true) or weekly (false).
Default: true

#### `backup_dir`
The location where backups are stored.  
Default: '/backup'

#### `solr_url`
The base url for downloading solr.
Default: 'http://apache.mirrors.lucidnetworks.net/lucene/solr'

#### `solr_version`
The version of solr to install.
Default: '5.0.3'

#### `solr_schema_version`
The version of the solr schema to use.
Valid options:
 - '1.2'
 - '1.3'
 - '1.4'
 - '2.0'
 - 'spatial' - configures solar with the spatial extensions.
                 Only supports bounding box.
 - 'spatial-ext' - configures solar with the extended spatial extension.
                    This allows for bounding box, point, and Polygon.
 - 'default'

The options correspond to the following structure.
```
 /usr/lib/ckan/default/src/ckan/ckan/config/solr/schema-<solr_schema_version>
```
The only exception is default which means schema.xml (required as of ckan 2.3).

#### `jts_url`
The url to be used to download the jts library for solr spatial ext.
Only used if spatial-ext option is set.
Default: uses version 1.13

#### `locale_default`
Use this to specify the locale (language of the text) displayed in the CKAN Web UI.
Default: 'en'

#### `i18n_directory`
By default, the locales are searched for in the ckan/i18n directory.
Use this option if you want to use another folder.
Default: ''

#### `ckan_storage_path`
The location where files will be stored for the file store.
Note, this module handles creating the directory; however, ensure the
path leading up to the directory has already been created.
Default: '/var/lib/ckan/default'

#### `display_timezone`
By default, all datetimes are considered to be in the UTC timezone. 
Use this option to change the displayed dates on the frontend.
Internally, the dates are always saved as UTC.
This option only changes the way the dates are displayed. 
The valid values for this options [can be found at pytz](http://pytz.sourceforge.net/#helpers) 
Available from CKAN 2.5+ (has no effect on previous versions).
Default: UTC

#### [`feeds_author_name`](http://docs.ckan.org/en/latest/maintaining/configuration.html#ckan-feeds-author-name)
This controls the feed author’s name. If unspecified, it’ll use ckan.site_id.  
Default: ''

#### [`feeds_author_link`](http://docs.ckan.org/en/latest/maintaining/configuration.html#ckan-feeds-author-link)
This controls the feed author’s link. If unspecified, it’ll use ckan.site_url.  
Default: ''

#### [`feeds_authority_name`](http://docs.ckan.org/en/latest/maintaining/configuration.html#ckan-feeds-authority-name)
The domain name or email address of the default publisher of the feeds and elements. If unspecified, it’ll use ckan.site_url.  
Default: ''

#### [`feeds_date`](http://docs.ckan.org/en/latest/maintaining/configuration.html#ckan-feeds-date)
A string representing the default date on which the authority_name is owned by the publisher of the feed.  
Default: ''

**Extensions**
The following are classes that utilize the ckan modularization framework known as [extensions](http://extensions.ckan.org).
To fully realize the plugin, puppet needs to be run twice.  The first time puppet runs,
the extension won't be added to ckan due to how this module manages the plugins.

### [ckan::ext::archiver](https://github.com/ckan/ckanext-archiver)
Installs the archiver extension.
** Parameters within `ckan::ext::archiver`:**

#### `ckan_conf`
Default: $ckan::params::ckan_conf

#### `paster`
Default: $ckan::params::paster

### [ckan::ext::datastore](http://docs.ckan.org/en/latest/maintaining/datastore.html)
Configures the datastore extension as its built-in.
By default, the configuration for postgresql is already handled,
so there is no need to specify it in this module.
So there are no parameters.

### [ckan::ext::dcat](https://github.com/ckan/ckanext-dcat)
Installs the dcat extension.
There are no parameters to specify.

### [ckan::ext::galleries](https://github.com/DataShades/ckan-galleries)
Installs the "galleries" extension which allows storing and referencing image and video assets.
There are no parameters to specify.

### [ckan::ext::geoview](https://github.com/ckan/ckanext-geoview)
Installs the geoview extension.

#### Requirements
In order for this module to update the default_views parameter, it MUST be declared before the ckan module in the manifests.
This will allow all of the variables to be evaluated.

Also, ensure that resource_proxy plugin has been enabled.

**Parameters within `ckan::ext::geoview`:**

####  `openlayers_formats`
A string seperated by spaces.
Available formats:

 - Web Map Service (WMS)      wms
 - Web Feature Service (WFS)  wfs
 - GeoJSON                    geojson
 - GML                        gml
 - KML                        kml
 - ArcGIS REST API            arcgis_rest
 - Google Fusion Tables       gft (requires google_api_key)

Default: 'wms geojson'

####  `openlayers_hide_overlays`
Overlays won't be visible by default (only the base layer)  
Default: false

#### `openlayers_hoveron`
The feature data popup will be displayed when hovering on.  
Default: true

#### `openlayers_google_api_key`
The google api key required to render google fusion table resources.
See https://developers.google.com/fusiontables/docs/v1/using#APIKey  
Default: undef


### [ckan::ext::googleanalytics](https://github.com/ckan/ckanext-googleanalytics)
Installs the "googleanalytics" extension, which sends tracking data to Google
Analytics and retrieves statistics from Google Analytics and inserts them into
CKAN pages.

**Parameters within `ckan::ext::googleanalytics`:**
#### `id`
The Google Analytics tracking ID (usually of the form UA-XXXXXX-X).

#### `account`
The account name (e.g. example.com -- see the top-level item at
 https://www.google.com/analytics).

#### `username`
Google Analytics username.

#### `password`
Google Analytics password

#### `track_events`
Adds Google Analytics Event tracking.
Default: false

### [ckan::ext::harvest](https://github.com/ckan/ckanext-harvest)
Installs the harvest extension.

**Parameters within `ckan::ext::harvest`:**

#### `ckan_conf`
Default: $ckan::params::ckan_conf

#### `paster`
Default: $ckan::params::paster


### [ckan::ext::hierarchy](https://github.com/datagovuk/ckanext-hierarchy)
Installs the "hierarchy" extension from data.gov.uk, which allows
organizations to have parents, and displays them in a tree.

Note, untested so probably needs some development effort.

### [ckan::ext::lcrnz](https://github.com/okfn/ckanext-lcrnz)
Installs the Landcare Research extension.
Also installs the following dependency extensions:
 - ckan::ext::repeating
 - ckan::ext::ldap

**Parameters within `ckan::ext::lcrnz`:**
#### `uri`
The uri to the ldap server to connect.
Example: 'ldap://localhost:389'


#### `base_dn`
The ldap base dn to use for user authentication.
Example: 'ou=users,dc=landcareresearch,dc=co,dc=nz'

#### `organization_id`
If this is set, users that log in using LDAP will automatically get added
to the given organization.
You can get it from http://<CKAN URL/api/action/organization_show?id=<ORG NAME>

### [ckan::ext::ldap](https://github.com/okfn/ckanext-ldap)
Installs the "ldap" extension.

**Parameters within `ckan::ext::ldap`:**
#### `uri`
The uri to the ldap server to connect.
Example: 'ldap://localhost:389'

#### `base_dn`
The ldap base dn to use for user authentication.
Example: 'ou=users,dc=landcareresearch,dc=co,dc=nz'

#### `search_filter`
The filter for searching through identities in ldap.
Default: 'uid={login}'

#### `username`
The user name to use as a lookup.
Default: 'uid'

#### `email`
The field that contains the user's email address.
Default: 'email'

#### `fullname`
The field that contains the user's full name.
Default: cn'

#### `organization_role`
The role of the user when logged in through ldap.
Default: member

#### `organization_id`
If this is set, users that log in using LDAP will automatically get added
to the given organization.

To create the organisation specified in ckanext.ldap.organization.id use
the paste command:
```
 paster --plugin=ckanext-ldap ldap setup-org -c /etc/ckan/default/development.ini
```
### [ckan::pages](https://github.com/ckan/ckanext-pages)
Installs the pages extension.
There are no parameters to specify.

### [ckan::ext::pdfview](https://github.com/ckan/ckanext-pdfview)
Installs the "pdf_view" extension
Note: if you want to enable pdf views of documents outside of
the ckan instance, you will need to enable resource_proxy plugin.

### [ckan::ext::private_datasets](https://github.com/conwetlab/ckanext-privatedatasets)
This CKAN extension allows a user to create private datasets that only
certain users will be able to see. When a dataset is being created,
it's possible to specify the list of users that can see this dataset.

**Parameters within `ckan::ext::private_datasets`:**

#### `show_acquire_url_on_create`
To show the Acquire URL when the user is creating a dataset.  
Default: false

#### `show_acquire_url_on_edit`
To show the Acquire URL when the user is editing a dataset.  
Default: false

### [ckan::ext::repeating](https://github.com/open-data/ckanext-repeating)
Installs the "repeating" extension.

This extension provides a way to store repeating fields in CKAN datasets,
resources, organizations and groups.

### [ckan::ext::report](https://github.com/datagovuk/ckanext-report)
Installs the report extension.
This class has no parameters to be specified.

### [ckan::ext::showcase](https://github.com/ckan/ckanext-showcase)
Installs the showcase extension.
This class has no parameters to be specified.

### [ckan::ext::spatial](http://docs.ckan.org/projects/ckanext-spatial/en/latest/)
Installs the "spatial" extension, which allows for the association of datasets
with geographic locations, and for searches across datasets to be restricted
to a particular geographical area. Additionally, it provides some support for
previewing geographical datasets.

**Parameters within `ckan::ext::spatial`:**

#### `template`
The name the template that spatial will modify.
Note, this will overwrite the following files:

* templates/package/search.html
* templates/package/read_base.html

#### `default_extent`
Sets the default extent for the map.  Should be a string in the format:

```
'[[15.62, -139.21], [64.92, -61.87]]'
```

or GeoJSON

```
   '{ \"type\":
    \"Polygon\", \"coordinates\": [[[74.89, 29.39],[74.89, 38.45], [60.50,
    38.45], [60.50, 29.39], [74.89, 29.39]]]}'
```

If undefined, will not set an extent.
Default: undef

### [ckan::ext::viewhelpers](https://github.com/ckan/ckanext-viewhelpers)
Installs the viewhelpers extension.
This class has no parameters to be specified.

## Manual Configuration

A systems admin account is still required to be created manually.  If using Ubuntu, use the following command:

```
/usr/local/bin/ckan_create_admin.bash <username>
```

## Usage

This section shows example uses of the ckan module.

### Example 1
This example demonstrates the most basic usage of the ckan module.

```
class { 'ckan':
  site_url              => 'test.ckan.com',
  site_title            => 'CKAN Test',
  site_description      => 'A shared environment for managing Data.',
  site_intro            => 'A CKAN test installation',
  site_about            => 'Pilot data catalogue and repository.',
  plugins               => 'stats text_preview recline_preview datastore resource_proxy pdf_preview',
  app_instance_id       => '{xxxxxxxxxxxxxxxx}',
  beaker_secret         => 'xxxxxxxxxxxx',
  is_ckan_from_repo     => 'false',
  ckan_package_url      => 'http://packaging.ckan.org/python-ckan_2.2_amd64.deb',
  ckan_package_filename => 'python-ckan_2.2_amd64.deb',
}
```

### Example 2
This example demonstrates a customize the ckan module.


Declaring a class that manages the configuration files.

```
class {landcare_ckan::config: }
```

Declaring the ckan module with the customized parameters.

```
class { 'ckan':
  site_url              => 'test.ckan.zen.landcareresearch.co.nz',
  site_title            => 'Landcare Research Test CKAN Repository',
  site_description      => 'A shared environment for managing Landcare Research Data.',
  site_intro            => 'Welcome to the Landcare Research Pilot Data Repository. This is a trial installation of the CKAN software, for us to test ahead of (all going well) a wider company rollout.',
  site_about            => 'Pilot data catalogue and repository for [Landcare Research] (http://www.landcareresearch.co.nz)',
  plugins               => 'stats text_preview recline_preview datastore resource_proxy pdf_preview',
  app_instance_id       => '{xxxxxxxxxxxxxxxx}',
  beaker_secret         => 'xxxxxxxxxxxx',
  site_logo             => $landcare_ckan::config::logo_src,
  license               => $landcare_ckan::config::license_src,
  is_ckan_from_repo     => false,
  ckan_package_url      => 'http://packaging.ckan.org/python-ckan_2.2_amd64.deb',
  ckan_package_filename => 'python-ckan_2.2_amd64.deb',
  custom_css            => $landcare_ckan::config::css_src,
  custom_imgs           => $landcare_ckan::config::custom_images_array,
  require               => Class['landcare_ckan::config'],
}
```

Class where the customized configuration files are managed
```
class landcare_ckan::config {
  $img_dir 			   = '/usr/lib/ckan/default/src/ckan/ckan/public/base/images'
  $landcare_src 	   = 'puppet:///modules/landcare_ckan'
  $css_src 			   = "$landcare_src/custom.css"
  $background_img_src  = "$landcare_src/LCR-ckan-homepage-background.jpg"
  $custom_images_array = [$background_img_src]
  $logo_filename 	   = 'lcr_logo_white_sm.png'
  $logo_src 		   = "$landcare_src/$logo_filename"
  $license  		   = 'NZ_licenses_ckan.json'
  $license_src 		   = "$landcare_src/$license"
  $backup_dir          = '/backup',
}
```

## Deploying with Vagrant
Vagrant can be used to easily deploy the ckan module for testing or production environments.
Vagrant was used for the development of the ckan module.  

### Vagrantfile
The following content should be copied to a clean Vagrantfile. 
Note, make sure to edit puppet.module_path with a path to
where the ckan module and the ckan module dependencies are located.

```
# -*- mode: ruby -*-
# vi: set ft=ruby :
Vagrant.configure("2") do |config|
  config.vm.box = "precise64"
  config.vm.box = "ubuntu/precise64"

  config.vm.network "private_network", ip: "192.168.33.10"
  config.vm.provider "virtualbox" do |v| 
    v.memory = 2048
    v.cpus = 1 
  end 

  config.vm.provision :shell, :path => "upgrade-puppet.sh"

  config.vm.provision "puppet" do |puppet|
    puppet.module_path = "</path to modules>/modules/"
    puppet.manifests_path = "manifests"
    puppet.manifest_file  = "test-ckan.pp"
  end 
end
```

### test-ckan.pp
This is the file that contains the declaration of the ckan module.
The file test-ckan.pp should be created in project_home/manifests/.

```
class { 'ckan':
  site_url              => 'http://192.168.33.10',
  site_title            => 'CKAN Test',
  site_description      => 'A shared environment for managing Data.',
  site_intro            => 'A CKAN test installation',
  site_about            => 'Pilot data catalogue and repository.',
  plugins               => 'stats text_preview recline_preview datastore resource_proxy pdf_preview',
  app_instance_id       => '{xxxxxxxxxxxxxxxx}',
  beaker_secret         => 'xxxxxxxxxxxx',
  is_ckan_from_repo     => false,
  ckan_package_url      => 'http://packaging.ckan.org/python-ckan_2.2_amd64.deb',
  ckan_package_filename => 'python-ckan_2.2_amd64.deb',
  pg_hba_conf_defaults  => true,
}
```

### upgrade-puppet.sh
This file manages installing the latest puppet from puppetlabs and updates apt-get
The file upgrade-puppet.sh should be created in project_home/ (same directory as the Vagrantfile).

```
#!/bin/bash

DISTRIB_CODENAME=$(lsb_release --codename --short)
DEB="puppetlabs-release-${DISTRIB_CODENAME}.deb"
DEB_PROVIDES="/etc/apt/sources.list.d/puppetlabs.list" # Assume that this file's existence means we have the Puppet Labs repo added

if [ ! -e $DEB_PROVIDES ]
then
    apt-get install --yes lsb-release
    # Print statement useful for debugging, but automated runs of this will interpret any output as an error
    # print "Could not find $DEB_PROVIDES - fetching and installing $DEB"
    wget -q http://apt.puppetlabs.com/$DEB
    sudo dpkg -i $DEB
    sudo apt-get update
    sudo apt-get install --yes puppet
fi
```

### Usage
In the project directory (vagrant directory),
* Run vagrant: vagrant up
* Wait for vagrant to finish deploying and installing ckan
Basically, wait for the console to return.  You will see the following line:

notice: Finished catalog run in 648.91 seconds

* Open a web browser and enter the following url
http://192.168.33.10

## Limitations

Only works with debian based OS's.

## Development

The module is Free and Open Source Software. Its available on bitbucket.  Please fork!