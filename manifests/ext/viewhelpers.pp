# == Class: ckan::ext::viewhelpers
#
# Installs the viewhelpers extension.
#
# See the plugin documentation for full details:
#
#   https://github.com/ckan/ckanext-viewhelpers
#
#
class ckan::ext::viewhelpers {

  anchor{'ckan::ext::viewhelpers::begin':}

  ckan::ext { 'viewhelpers':
    plugin   => 'viewhelpers',
    revision => 'master',
    require  => Anchor['ckan::ext::viewhelpers::begin'],
  }

  anchor{'ckan::ext::viewhelpers::end':
    require => Ckan::Ext['viewhelpers'],
  }
}
