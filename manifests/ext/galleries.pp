# == Class: ckan::ext::galleries
#
# Installs the "galleries" extension which allows
# storing and referencing image and video assets.
#
# See the plugin documentation for full details:
#
#   https://github.com/DataShades/ckan-galleries
#
# == Parameters ==
# This extension doesn't define any additional config settings.
#
class ckan::ext::galleries {
  anchor{'ckan::ext::galleries::begin':}

  $packages = ['libmysqlclient-dev','libjpeg-dev','python-dev']

  ensure_packages($packages)

  # required and will fail without it.
  file {'/var/www/.flickr':
    ensure  => directory,
    owner   => 'www-data',
    group   => 'www-data',
    require => [Anchor['ckan::ext::galleries::begin'],
                  Package['libmysqlclient-dev']],
  }

  ckan::ext { 'galleries':
    source   => 'git://github.com/DataShades/ckan-galleries.git',
    revision => 'master',
    plugin   => 'dfmp',
    require  => File['/var/www/.flickr'],
  }

  exec { 'setup_galleries':
    command     => "${ckan::python} setup.py install",
    cwd         => $ckan::ext::galleries::extdir,
    refreshonly => true,
    subscribe   => Ckan::Ext['galleries'],
  }

  anchor{'ckan::ext::galleries::end':
    require => Exec['setup_galleries'],
  }
}