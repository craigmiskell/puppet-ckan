# == Class: ckan::redis
#
# A simple non-configured redis server install, mainly for the harvest extension
# There's too many redis modules in the forge to make a good assessment or to
# enforce the use of any particular one of them for CKAN.  Enhancement
# possibilities exist in making this optional in the harvest module
# (e.g. supply your own).
#
# == Examples 
#
# include ckan::redis
#
# === Copyright
#
# GPL-3.0+
#
class ckan::redis {
  anchor{'ckan::redis::begin':}

  package { 'redis-server':
    ensure  => 'installed',
    require => Anchor['ckan::redis::begin'],
  }

  anchor{'ckan::redis::end':
    require => Package['redis-server'],
  }
}
