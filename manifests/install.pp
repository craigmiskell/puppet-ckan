# == Class ckan::install
#
# installs ckan
# details: http://docs.ckan.org/en/ckan-2.0/install-from-package.html
#
class ckan::install {

  anchor{'ckan::install::begin':}

  ## Variables

  # determine ckan
  if $ckan::ckan_version == undef {
    # report error
    fail('ckan_version not set!')
  }

  # use ckan.org's package repository if not specified
  if $ckan::ckan_package_url == undef and
    $ckan::ckan_package_filename == undef {
    $ckan_package_filename =
    "python-ckan_${ckan::ckan_version}-${::lsbdistcodename}_amd64.deb"
    $ckan_package_url = "http://packaging.ckan.org/${ckan_package_filename}"
  }else {
    $ckan_package_filename = $ckan::ckan_package_filename
    $ckan_package_url = $ckan::ckan_package_url
  }

  # determine solr
  if $ckan::solr_schema_version == 'default' {
    $solr_schema_path = "${ckan::ckan_src}/ckan/config/solr/schema.xml"
  } elsif $ckan::solr_schema_version == 'spatial'  or
          $ckan::solr_schema_version == 'spatial-ext'{
    # download file
    file{"${ckan::ckan_default}/schema.xml":
      ensure  => file,
      content => template('ckan/ext/schema.xml.erb'),
      require => File[$ckan::ckan_default],
      before  => Class['solr'],
    }
    $solr_schema_path = "${ckan::ckan_default}/schema.xml"
  } else {
    $solr_schema_path = "${ckan::ckan_src}/ckan/config/solr/\
schema-${ckan::solr_schema_version}.xml"
  }

  class {'wget':
    require => Anchor['ckan::install::begin'],
  }

  # setup the ubuntugis as we need to use postgresql 9.3!
  # in order for compatiblity of the spatial search
  package{'python-software-properties':
    require => Class['wget'],
  }

  # Install CKAN deps
  package { $ckan::required_packages:
    ensure  => installed,
    require => Package['python-software-properties'],
  }

  exec { 'a2enmod wsgi':
    command => $ckan::wsgi_command,
    creates => $ckan::wsgi_creates,
    require => Package[$ckan::required_packages],
  }

  # Git is necessary for installing extensions
  package { 'git-core':
    ensure  => present,
    require => Exec['a2enmod wsgi'],
  }

  # Install CKAN either from APT repositories or from the specified file
  if $ckan::is_ckan_from_repo == true {
    package { 'python-ckan':
      ensure  => latest,
      before  => File['/opt/ckan_plugin_collector'],
      require => Package['git-core'],
    }
  } else {
    file { $ckan::ckan_package_dir:
      ensure  => directory,
      require => Package['git-core'],
    }
    wget::fetch { 'ckan package':
      source      => $ckan_package_url,
      destination => "${ckan::ckan_package_dir}/${ckan_package_filename}",
      verbose     => false,
      require     => File[$ckan::ckan_package_dir],
    }
    package { 'python-ckan':
      ensure   => latest,
      provider => dpkg,
      source   => "${ckan::ckan_package_dir}/${ckan_package_filename}",
      before   => File['/opt/ckan_plugin_collector'],
      require  => Wget::Fetch['ckan package'],
    }
  }

  # dependancy cycle with nodejs and postgis
  class {'ckan::install::nodejs':
    before  => File['/opt/ckan_plugin_collector'],
    require => Package['python-ckan'],
  }

  class {'postgis':
    postgres_pass        => $ckan::postgres_pass,
    postgis_version      => $ckan::postgis_version,
    pg_hba_conf_defaults => $ckan::pg_hba_conf_defaults,
    pg_hba_rules         => $ckan::pg_hba_rules,
    require              => Package['python-ckan'],
  }

  # setup the plugin collector
  file {'/opt/ckan_plugin_collector':
    ensure  => directory,
    require => Class['postgis'],
  }

  file {'/opt/ckan_plugin_collector/plugin_collector.bash':
    ensure  => file,
    source  => 'puppet:///modules/ckan/plugin_collector.bash',
    mode    => '0755',
    require => File['/opt/ckan_plugin_collector'],
  }

  # add configuration directories
  file { [$ckan::ckan_etc, $ckan::ckan_default]:
    ensure  => directory,
    require => File['/opt/ckan_plugin_collector/plugin_collector.bash'],
  }

notice("solr version = ${ckan::solr_version}")
  # Install Solr/Jetty
  class{'solr':
    url     => $ckan::solr_url,
    version => $ckan::solr_version,
    timeout => 0, # disable timeout
    require => File[$ckan::ckan_default],
  }

  # Jetty configuration
  # Note, internally to core, it notifies class solor so the require is
  # not necessary.
  solr::core{'ckan':
    schema_src_file => $solr_schema_path,
    #require         => Class['solr'],
  }

  # Disable status module in apache, because its proxied from
  # nginx thus "require local" has no effect
  file { '/etc/apache2/mods-enabled/status.conf':
    ensure => 'absent',
    notify => Service[$ckan::apache_service],
  }
  file { '/etc/apache2/mods-enabled/status.load':
    ensure => 'absent',
    notify => Service[$ckan::apache_service],
  }

  anchor{'ckan::install::end':
    require => Solr::Core['ckan'],
  }
}
