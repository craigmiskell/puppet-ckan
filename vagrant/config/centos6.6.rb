module MyVars
  OS       = "puppetlabs/centos-6.6-64-puppet"
  OS_URL   = "https://atlas.hashicorp.com/puppetlabs/boxes/centos-6.6-64-puppet"
  PUPPET   = "scripts/noop.sh"
  RPM_URL  = "http://yum.postgresql.org/8.4/redhat/rhel-6-x86_64/pgdg-centos-8.4-3.noarch.rpm"
  RPM_NAME = "pgdg-centos-8.4-3.noarch"
end

