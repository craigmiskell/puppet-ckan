# == Class: ckan::ext::private_datasets
#  This CKAN extension allows a user to create private datasets that only
#  certain users will be able to see. When a dataset is being created,
#  it's possible to specify the list of users that can see this dataset.
#  https://github.com/conwetlab/ckanext-privatedatasets
#
# === Parameters
#
# [*show_acquire_url_on_create*]
#   To show the Acquire URL when the user is creating a dataset.
#   Default: false
#
# [*show_acquire_url_on_edit*]
#   To show the Acquire URL when the user is editing a dataset
#   Default: false
#
class ckan::ext::private_datasets (
  $show_acquire_url_on_create = false,
  $show_acquire_url_on_edit   = false,
){
  ckan::ext { 'privatedatasets':
    source   => 'git://github.com/conwetlab/ckanext-privatedatasets.git',
    revision => 'master',
    plugin   => 'privatedatasets'
  }
  $parser = 'ckanext.privatedatasets.parsers.fiware:FiWareNotificationParser'
  concat::fragment { 'privatedatasets':
    target  => '/etc/ckan/default/production.ini',
    content => "
# Private Datasets Extension
ckan.privatedatasets.parser                     = ${parser}
ckan.privatedatasets.show_acquire_url_on_create = ${show_acquire_url_on_create}
ckan.privatedatasets.show_acquire_url_on_edit   = ${show_acquire_url_on_edit}
",
  }
}