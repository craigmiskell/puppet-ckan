# == Type: ckan::pip_package
#
# Install an extra pip module, typically to work around mistakes/limitations
# in extensions.
#
# == Parameters ==
#
# [*version*]
#   Specify the version of the package.
#   Note, its advised to leave as undef.
#   Default: undef
#
define ckan::pip_package (
  $version = undef,
  ) {

  anchor{"ckan::pip_package::${version}::begin":}

  $base_command = "/usr/lib/ckan/default/bin/pip install ${name}"

  $command = $version ? {
    undef   => $base_command,
    default => "${base_command}=${version}"
  }

  exec { "ckan pip ${name}":
    command => $command,
    unless  => "/usr/lib/ckan/default/bin/pip list|/bin/grep -q ${name}",
    require => [Class['ckan::install'],
              Anchor["ckan::pip_package::${version}::begin"]],
  }

  anchor{"ckan::pip_package::${version}::end":
    require => Exec["ckan pip ${name}"],
  }
}
