#!/bin/bash
# backs up the database

. /usr/lib/ckan/default/bin/activate
cd /usr/lib/ckan/default/src/ckan
paster --plugin=ckan search-index rebuild --config=/etc/ckan/default/production.ini
paster views create image_view recline_view webpage_view pdf_view geojson_view text_view --config=/etc/ckan/default/production.ini