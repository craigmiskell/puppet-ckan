# == Class: ckan::ext::showcase
#
# Installs the showcase extension.
#
# See the plugin documentation for full details:
#
#   https://github.com/ckan/ckanext-showcase
#
#
class ckan::ext::showcase {

  anchor {'ckan::ext::showcase::begin':}

  ckan::ext { 'showcase':
    plugin   => 'showcase',
    revision => 'master',
    require  => Anchor['ckan::ext::showcase::begin'],
  }

  anchor {'ckan::ext::showcase::end':
    require => Ckan::Ext['showcase'],
  }
}
