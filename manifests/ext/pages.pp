# == Class: ckan::pages
#
# Installs the pages extension.
#
# See the plugin documentation for full details:
#
#   https://github.com/ckan/ckanext-pages
#
class ckan::ext::pages {
  anchor{'ckan::ext::pages::begin':}

  ckan::ext { 'pages':
    plugin   => 'pages',
    revision => 'master',
    require  => Anchor['ckan::ext::pages::begin'],
  }

  anchor{'ckan::ext::pages::end':
    require => Ckan::Ext['pages'],
  }
}