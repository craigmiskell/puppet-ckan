# == Class: ckan::ext::spatial
#
# Installs the "spatial" extension, which allows for the association of datasets
# with geographic locations, and for searches across datasets to be restricted
# to a particular geographical area. Additionally, it provides some support for
# previewing geographical datasets.
#
# See the plugin documentation for full details:
#
#   http://docs.ckan.org/projects/ckanext-spatial/en/latest/
#
# == Parameters ==
#
# [*template*]
#   The name the template that spatial will modify.
#   Note, this will overwrite the following files:
#   * templates/package/search.html
#   * templates/package/read_base.html
#
# [*default_extent*]
#   Sets the default extent for the map.  Should be a string in the format:
#
#   '[[15.62, -139.21], [64.92, -61.87]]'
#
#    or GeoJSON
#
#   '{ \"type\":
#    \"Polygon\", \"coordinates\": [[[74.89, 29.39],[74.89, 38.45], [60.50,
#    38.45], [60.50, 29.39], [74.89, 29.39]]]}'
#
#   If undefined, will not set an extent.
#   Default: undef
#
class ckan::ext::spatial (
  $template,
  $default_extent = undef,
){
  anchor {'ckan::ext::spatial::begin': }

  $search    = "${ckan::ckan_ext}/ckanext-${template}/ckanext/${template}/\
templates/package/search.html"

  $read_base = "${ckan::ckan_ext}/ckanext-${template}/ckanext/${template}/\
templates/package/read_base.html"

  if $default_extent {
    $use_default_extent = true
  }else {
    $use_default_extent = false
  }

  $required_packages = [
    'python-dev',
    'libxml2-dev',
    'libxslt1-dev',
    'libgeos-c1',
  ]
  ensure_packages($required_packages)

  # check if need to install external libraries for solr
  if $ckan::solr_schema_version == 'spatial-ext'{
    solr::shared_lib{'jts':
      url     => $ckan::jts_url,
      require => Class['solr']
    }
    solr::shared_lib{'mmseg4j':
      url     => 'http://central.maven.org/maven2/com/chenlb/mmseg4j/mmseg4j-solr/2.3.0/mmseg4j-solr-2.3.0.jar',
      require => Class['solr']
    }
    $backend = 'solr-spatial-field'
  }else {
    $backend = 'solr'
  }

  # populate
  # turn on psql for database
  check_run::task{'init_db_spatial':
    exec_command => "/usr/bin/psql -d ckan_default -c\
 'CREATE EXTENSION postgis;'",
    user         => 'postgres',
    require      => [Anchor[ 'ckan::ext::spatial::begin'],
                Postgresql::Server::Database['ckan_default'],
    ],
  }

  check_run::task{'init_db_topology':
    exec_command => "/usr/bin/psql -d ckan_default -c\
 'CREATE EXTENSION postgis_topology;'",
    user         => 'postgres',
    require      => Check_run::Task['init_db_spatial'],
  }

  check_run::task{'change_db_perms':
    exec_command => "/usr/bin/psql -d ckan_default -c\
 'GRANT ALL PRIVILEGES ON SCHEMA topology TO GROUP ckan_default;' &&\
 /usr/bin/psql -d ckan_default -c 'GRANT ALL PRIVILEGES ON ALL TABLES IN\
 SCHEMA topology TO GROUP ckan_default;'",
    user         => 'postgres',
    require      => Check_run::Task['init_db_topology'],
  }

  ckan::ext { 'spatial':
    plugin  => 'spatial_metadata spatial_query',
    require => Check_run::Task['change_db_perms'],
  }

  concat::fragment { 'ckanext-spatial':
    target  => '/etc/ckan/default/production.ini',
    content => "
# Spatial Extension
ckanext.spatial.search_backend = ${backend}
",
    order   => 02,
  }

  # add spatial search widget
  file{$search:
    ensure  => file,
    content => template('ckan/ext/search.html.erb'),
    require => Package['python-ckan'],
    before  => Anchor['ckan::ext::spatial::end'],
  }
  file {$read_base:
    ensure  => file,
    content => template('ckan/ext/read_base.html.erb'),
    require => File[$search],
    before  => Anchor['ckan::ext::spatial::end'],
  }
  anchor {'ckan::ext::spatial::end':
    require => Ckan::Ext['spatial'],
  }
}
