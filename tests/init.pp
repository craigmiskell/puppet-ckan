# -- main stage --
class { 'ckan':
  site_url             => 'http://192.168.33.10',
  site_title           => 'CKAN Test',
  site_description     => 'A shared environment for managing Data.',
  site_intro           => 'A CKAN test installation',
  site_about           => 'Pilot data catalogue and repository.',
  plugins              =>
  'stats text_preview recline_view datastore resource_proxy datapusher',
  app_instance_id      => 'pass',
  beaker_secret        => 'pass',
  is_ckan_from_repo    => false,
  max_resource_size    => '1000',
  max_image_size       => '15',
  datapusher_formats   => "csv xls xlsx application/csv\
 application/vnd.ms-excel\
 application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
  pg_hba_conf_defaults => true,
  install_ckanapi      => true,
  ckan_version         => '2.5',
  solr_version         => '5.5.2',
  solr_schema_version  => 'spatial',
  display_timezone     => 'Pacific/Auckland',
}

# test for the new galleries plugin
class {'ckan::ext::galleries': }

#class {'ckan::ext::pdfview': }
#class {'ckan::ext::lcrnz':
#  uri             => 'ldap://openldap.landcareresearch.co.nz:389',
#  organization_id => '',
#  base_dn         => 'ou=users,dc=landcareresearch,dc=co,dc=nz',
#}

#class {'ckan::ext::spatial':
#  template => 'lcrnz',
#}

#class {'ckan::ext::spatial': }
#class {'ckan::ext::private_datasets': }
